/**
 * Created by ivan on 14.03.17.
 */
import {Exchange} from "./exchange"

export const EXCHANGES: Exchange[] = [
  {name:"Bitcoin", price:50, change: 1},
  {name:"New Bitcoin", price:100, change: 0}
];
