/**
 * Created by ivan on 14.03.17.
 */
export class Exchange {
  name: String;
  price: number;
  change: number;
}
