import {Exchange} from "./exchange";
import {Injectable} from "@angular/core";
import "rxjs/add/operator/toPromise";
import {Http} from "@angular/http";

@Injectable()
export class ExchangeService {

  private exchangeApi = "http://localhost:5050/api/current";

  constructor(private http:Http){}

  getExchanges(): Promise<Exchange[]> {
    return this.http.get(this.exchangeApi)
      .toPromise()
      .then(response => response.json() as Exchange[])
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any>{
    console.error('An error occured', error);
    return Promise.reject(error.message || error);
  }
}
