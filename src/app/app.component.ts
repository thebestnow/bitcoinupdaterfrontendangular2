import {Component, OnInit} from '@angular/core';
import {Exchange} from "./exchange";
import {ExchangeService} from "./exchange.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit{
  exchanges: Exchange[];

  title = 'app works!';
  constructor(private exchangeService:ExchangeService){}

  getExchanges():void{
    this.exchangeService.getExchanges().then(exchanges => this.exchanges = exchanges);
  }

  ngOnInit(): void {
    this.getExchanges();
  }
}
